package com.lf.booksearch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkUtils {

    private static final String URL_BASE = "https://www.googleapis.com/books/v1/volumes?q=isbn:";

    static String getBookInfo(String isbn_code){
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String bookJSONString = null;

        try{

            String url = URL_BASE+isbn_code;

            URL requestURL = new URL(url);

            urlConnection = (HttpURLConnection) requestURL.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();


            InputStream inputStream = urlConnection.getInputStream();//html
            StringBuffer buffer = new StringBuffer();

            if(inputStream == null){
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while((line = reader.readLine()) != null){
                buffer.append(line + "\n");
            }

            if(buffer.length() == 0)
                return null;

            bookJSONString = buffer.toString();

        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
        finally {
            if(urlConnection != null)
                urlConnection.disconnect();

            if(reader != null){
                try{
                    reader.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }

            return bookJSONString;
        }

    }
}

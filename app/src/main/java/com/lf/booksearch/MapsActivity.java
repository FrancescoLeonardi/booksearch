package com.lf.booksearch;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FirebaseFirestore db;
    private LatLng lastNode;
    private Bitmap smallMarker;

    private double CONSTAINT_BOUND = 20.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        Double lat = getIntent().getDoubleExtra("LAT", 0);
        Double lon = getIntent().getDoubleExtra("LON", 0);

        lastNode = new LatLng(lat, lon);
        resizeBitmap();

        db = FirebaseFirestore.getInstance();
        mapFragment.getMapAsync(this);
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(lastNode != null) {
            mMap.addMarker(new MarkerOptions()
                    .position(lastNode)
                    .icon(BitmapDescriptorFactory.defaultMarker())
                    .title("You are here"));
            bounds();
        }

        db.collection("books")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                Book b = document.toObject(Book.class);
                                if(b!=null && b.lat != null && b.lon != null && b.lat != 0 && b.lon != 0){
                                    LatLng node = new LatLng(b.lat, b.lon);
                                    mMap.addMarker(new MarkerOptions()
                                            .position(node)
                                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                                            .snippet(b.tagged)
                                            .title(b.title));
                                    if(lastNode == null)
                                        lastNode = node;
                                }
                            }

                             bounds();
                        }
                    }
                });
    }

    private void bounds(){
        LatLngBounds boundNode = new LatLngBounds(new LatLng(lastNode.latitude - CONSTAINT_BOUND, lastNode.longitude - CONSTAINT_BOUND), new LatLng(lastNode.latitude + CONSTAINT_BOUND, lastNode.longitude + CONSTAINT_BOUND));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(boundNode.getCenter(), 10));
    }

    private void resizeBitmap(){
        int height = 70;
        int width = 70;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.openbook_freepik);
        Bitmap b=bitmapdraw.getBitmap();
        smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
    }

}

package com.lf.booksearch;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

public class BookActivity extends AppCompatActivity {

    private FirebaseFirestore db;

    private TextView isbn, title, author, page, rating, language;
    private LinearLayout layTitle, layAuthor, layPage, layRating, layLanguage;
    private ImageView imgBook;
    private String passISBN;
    private ProgressBar progressBar;

    final String TAG = "BookActivity";

    public BookActivity(){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_detail);

        db = FirebaseFirestore.getInstance();

        isbn = findViewById(R.id.txtISBN);
        title = findViewById(R.id.txtTitle);
        author = findViewById(R.id.txtAuthor);
        page = findViewById(R.id.txtPage);
        rating = findViewById(R.id.txtAverage);
        language = findViewById(R.id.txtLanguage);

        layTitle = findViewById(R.id.layTitle);
        layAuthor = findViewById(R.id.layAuthor);
        layPage = findViewById(R.id.layPage);
        layRating = findViewById(R.id.layRating);
        layLanguage = findViewById(R.id.layLanguage);

        progressBar = findViewById(R.id.progressBar);

        imgBook = findViewById(R.id.imgBook);

    }



    private void loadData(){

        DocumentReference docRef = db.collection("books").document(passISBN);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Book b = document.toObject(Book.class);
                    if (document.exists()) {

                        isbn.setText( document.getId() );

                        if(b.title.length() > 0)
                            title.setText(b.title);
                        else
                            layTitle.setVisibility(View.GONE);

                        if(b.author.length() > 0)
                            author.setText(b.author.replace('[', ' ').replace('"',' ').replace(']', ' '));
                        else
                            layAuthor.setVisibility(View.GONE);

                        if(b.page >= 0)
                            page.setText(Long.toString(b.page));
                        else
                            layPage.setVisibility(View.GONE);

                        if(b.averageRating >= 0)
                            rating.setText(Double.toString(b.averageRating) + " / 5.0");
                        else
                            layRating.setVisibility(View.GONE);

                        if(b.language.length() > 0)
                            language.setText(b.language);
                        else
                            layLanguage.setVisibility(View.GONE);

                        if(b.thumbnail != null && b.thumbnail.length() > 0)
                            Picasso.with(getApplicationContext()).load(b.thumbnail).into(imgBook);

                    } else {
                            Log.e(TAG, "No such document");
                    }
                } else {
                    Log.e(TAG, "get failed with ", task.getException());
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();


        passISBN = getIntent().getStringExtra("ISBN");

        BookActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                loadData();

            }
        });

    }


}

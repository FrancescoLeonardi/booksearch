package com.lf.booksearch;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.util.ArrayList;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;


//ES: https://www.googleapis.com/books/v1/volumes?q=isbn:9788871927985

public class MainActivity extends AppCompatActivity {

        private FirebaseAuth auth;
        private FirebaseFirestore db;
        private Button btnScan, btnHistory, btnSend, btnLogout, btnMaps;
        private EditText eISBN;

        private String nameUser;
        private Double latitude, longitude;

        ProgressBar progressBar;
        Intent toLogin, toHistory, toMaps;
        LocationTracker tracker;


    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toLogin = new Intent(this, LoginActivity.class);
        toHistory = new Intent(this, HistoryActivity.class);
        toMaps = new Intent(this, MapsActivity.class);

        permissions.add(INTERNET);
        permissions.add(ACCESS_NETWORK_STATE);
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        checkPermission();

        try {
            FirebaseApp.initializeApp(this);

            auth = FirebaseAuth.getInstance();

            btnSend = findViewById(R.id.btnSendISBN);
            btnScan = findViewById(R.id.btnScan);
            btnHistory = findViewById(R.id.btnHistory);
            btnLogout = findViewById(R.id.btn_logout);
            btnMaps = findViewById(R.id.btnMaps);
            eISBN = findViewById(R.id.edTxtISBN);

            progressBar = findViewById(R.id.progressBar);

        }
        catch (NoSuchFieldError ex){
            startActivity(toLogin);
        }

        tracker=new LocationTracker(MainActivity.this);

        if(tracker.isLocationEnabled)
        {
            latitude=tracker.getLatitude();
            longitude=tracker.getLongitude();

            if(latitude == 0 && longitude == 0)
                showMessageOK("Ocean? Latitude and longitude are 0");
        }
        else
        {
            try {
                tracker.askToOnLocation();
            }
            catch(Exception ex){
                showMessageOK("Error GPS");
            }
        }


        checkConnection();

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (auth.getCurrentUser() == null) {
            startActivity(toLogin);
            finish();
        }
        else{
            nameUser = auth.getCurrentUser().getEmail();
            logged();
        }
    }

    private void logged(){
        try {
            final Activity activity = this;

            db = FirebaseFirestore.getInstance();

            btnScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IntentIntegrator integrator = new IntentIntegrator(activity);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setPrompt("Scan");
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(false);
                    integrator.initiateScan();
                }
            });

            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendISBN(eISBN.getText().toString());
                }
            });

            btnHistory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    toHistory.putExtra("ID",nameUser);
                    startActivity(toHistory);
                }
            });

            btnLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    auth.signOut();
                    FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
                        @Override
                        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        }
                    };
                    finish();
                    startActivity(toLogin);
                }
            });

            btnMaps.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    toMaps.putExtra("LAT",latitude);
                    toMaps.putExtra("LON",longitude);
                    startActivity(toMaps);
                }
            });

            progressBar.setVisibility(View.GONE);
        }
        catch (NoSuchFieldError ex){
            startActivity(toLogin);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                showMessageOK("Scanning terminated");
            } else {
                eISBN.setText(result.getContents());
                sendISBN(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    private void sendISBN(String ISBN){

        if(ISBN.length() != 0 && checkConnection()){

            final Intent intent = new Intent(this, BookActivity.class);
            new FindBook(db,
                        ISBN,
                        nameUser,
                        latitude,
                        longitude,
                        intent,
                       MainActivity.this
            ).execute(ISBN);
        }
        else
            showMessageOK("Enter a ISBN");


    }


    private boolean checkConnection(){

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected())
            return  true;
        else {

            showMessageOK("Please check your network connection and try again");
            return false;
        }

    }


    //region permessi

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();

    private final static int ALL_PERMISSIONS_RESULT = 101;

    public void checkPermission(){
        permissionsToRequest = findUnAskedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    //endregion

    private void showMessageOK(String message) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create()
                .show();
    }
}

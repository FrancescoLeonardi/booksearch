package com.lf.booksearch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    List<Book> books;
    ListView lst;

    ProgressBar progressBar;

    private FirebaseFirestore db;

    String id_book;
    final String TAG = "HistoryActivity";

    public HistoryActivity(){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        lst = findViewById(R.id.lstViewHistory);

        books = new ArrayList<>();

        db = FirebaseFirestore.getInstance();
        progressBar = findViewById(R.id.progressBar);

        id_book = getIntent().getStringExtra("ID");

        HistoryActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                loadData();
            }
        });
    }

    private void loadData(){
        db.collection("books")
                .whereEqualTo("tagged", id_book)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Book b = document.toObject(Book.class);
                                Log.d(TAG, b.title + b.tagged);
                                books.add(b);
                            }

                            ((ArrayAdapter) lst.getAdapter()).notifyDataSetChanged();
                        } else {
                            Log.e(TAG, "Error getting documents: ", task.getException());
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }


    @Override
    protected void onStart() {
        super.onStart();

        final Intent intent = new Intent(this, BookActivity.class);

        ListAdapter customAdapter = new ListAdapter(this, R.layout.row_book, books);
        lst.setAdapter(customAdapter);

        lst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                intent.putExtra("ISBN", books.get(position).isbn+id_book);
                startActivity(intent);

            }
        });
    }

    public class ListAdapter extends ArrayAdapter<Book> {

        private int resourceLayout;
        private Context mContext;

        public ListAdapter(Context context, int resource, List<Book> items) {
            super(context, resource, items);
            this.resourceLayout = resource;
            this.mContext = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(mContext);
                v = vi.inflate(resourceLayout, null);
            }

            Book p = getItem(position);

            if (p != null) {
                ImageView imgBook = v.findViewById(R.id.imgBook);
                TextView txtTitle = v.findViewById(R.id.titleBook);

                if (p.thumbnail != null && p.thumbnail.length() > 0) {
                    Picasso.with(mContext).load(p.thumbnail).into(imgBook);
                }

                if (p.title != null && p.thumbnail.length() > 0) {
                    txtTitle.setText(p.title);
                }

            }

            return v;
        }

    }
}

package com.lf.booksearch;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;

public class LocationTracker extends Service implements LocationListener {

    //region variables

    private final Context con;

    boolean isGPSOn=false;
    boolean isNetWorkEnabled=false;
    boolean isLocationEnabled=false;
    private static final long MIN_DISTANCE_TO_REQUEST_LOCATION=1; // metri
    private static final long MIN_TIME_FOR_UPDATES=1000*1; // millesimi

    Location location;
    double latitude,longitude;

    LocationManager locationManager;

    //endregion

    public LocationTracker(Context context)
    {
        this.con=context;
        checkIfLocationAvailable();
    }

    /**
     * Use for retrieving location if possible, if not show a error message into alert dialog
     * @return Location
     */
    public Location checkIfLocationAvailable()
    {
        try
        {
            locationManager=(LocationManager)con.getSystemService(LOCATION_SERVICE);
            isGPSOn=locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetWorkEnabled=locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if(!isGPSOn && !isNetWorkEnabled)
            {
                isLocationEnabled=false;
                showMessageOK("GPS is not available");
            }
            else {
                isLocationEnabled=true;

                if(isNetWorkEnabled)
                {
                    try {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_FOR_UPDATES, MIN_DISTANCE_TO_REQUEST_LOCATION, this);
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }catch(SecurityException se){
                        se.fillInStackTrace();
                    }
                }
                if(isGPSOn)
                {
                    try{
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_FOR_UPDATES,MIN_DISTANCE_TO_REQUEST_LOCATION,this);
                        if(locationManager!=null)
                        {
                            location=locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if(location!=null)
                            {
                                latitude=location.getLatitude();
                                longitude=location.getLongitude();
                            }
                        }
                    }catch(SecurityException se){
                        se.fillInStackTrace();
                    }
                }
            }
        }catch (Exception e)
        {
        }
        return location;
    }

    /**
     * Use for stopping the location manager
     */
    public void stopLocationTracker()
    {
        if(locationManager!=null)
        {
            locationManager.removeUpdates(LocationTracker.this);
        }
    }

    /**
     * Use for getting the latitude of location retrieved
     * @return double latitude
     */
    public double getLatitude()
    {
        if(location!=null)
        {
            latitude=location.getLatitude();
        }
        return latitude;
    }

    /**
     * Use for getting the longitude of location retrieved
     * @return double longitude
     */
    public double getLongitude()
    {
        if(location!=null)
        {
            longitude=location.getLongitude();
        }
        return longitude;
    }


    /**
     * Use for displaying the setting of gps
     */
    public void askToOnLocation()
    {
        try {
            AlertDialog.Builder dialog = new AlertDialog.Builder(con);

            dialog.setTitle("Settings");
            dialog.setMessage("GPS is not Enabled.Do you want to go to settings to enable it?");
            dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    con.startActivity(intent);
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            dialog.show();
        }
        catch(Exception ex){
            showMessageOK("Error GPS");
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        this.location=location;
    }
    @Override
    public void onProviderDisabled(String provider) {
    }
    @Override
    public void onProviderEnabled(String provider) {
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Use for displaying a message with button ok for close alert dialog
     * @param message the String to display
     */
    private void showMessageOK(String message) {
        new android.support.v7.app.AlertDialog.Builder(con)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create()
                .show();
    }

}
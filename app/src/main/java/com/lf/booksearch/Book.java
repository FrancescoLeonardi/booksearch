package com.lf.booksearch;

public class Book {
    public String isbn = "";
    public String title = "";
    public String author = "";
    public long page = -1;
    public double averageRating = -1.0;
    public String thumbnail = "";
    public String language = "";
    public Double lat = 0.0;
    public Double lon = 0.0;
    public String tagged = "";

    public Book(){
    }

    public Book(String ISBN,
                String title,
                String author,
                long page,
                double averageRating,
                String thumbnail,
                String language,
                Double lat,
                Double lon,
                String tagged){
        this.isbn = ISBN;
        this.title = title;
        this.author = author;
        this.page = page;
        this.averageRating = averageRating;
        this.thumbnail = thumbnail;
        this.language = language;
        this.lat = lat;
        this.lon = lon;
        this.tagged = tagged;
    }
}

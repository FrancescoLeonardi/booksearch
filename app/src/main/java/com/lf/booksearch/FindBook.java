package com.lf.booksearch;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONObject;

public class FindBook extends AsyncTask<String, Void, String> {

    Book book;

    private String ISBN;
    private String tagged;
    private Double lat, lon;
    private Intent intent;
    private Context context;
    private FirebaseFirestore db;

    public FindBook(FirebaseFirestore mDatabase, String ISBN, String tagged,Double lat, Double lon, Intent t, Context context){
        this.ISBN = ISBN;
        this.tagged = tagged;
        this.lat = lat;
        this.lon = lon;
        this.intent = t;
        this.context = context;
        this.db = mDatabase;
        book = new Book();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try{
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("items");

            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonBook = jsonArray.getJSONObject(i);

                JSONObject volumeInfo = jsonBook.getJSONObject("volumeInfo");

                try{
                    if(volumeInfo.has("title"))
                        book.title = volumeInfo.getString("title");

                    if(volumeInfo.has("authors"))
                        book.author = volumeInfo.getString("authors");

                    if(volumeInfo.has("pageCount"))
                        book.page = volumeInfo.getInt("pageCount");

                    if(volumeInfo.has("averageRating"))
                        book.averageRating = volumeInfo.getDouble("averageRating");

                    if(volumeInfo.has("imageLinks") ){

                        JSONObject imageLinks = volumeInfo.getJSONObject("imageLinks");
                        if(imageLinks.has("thumbnail"))
                            book.thumbnail = imageLinks.getString("thumbnail");
                    }

                    if(volumeInfo.has("language"))
                     book.language = volumeInfo.getString("language");


                    book.isbn = ISBN;
                    book.tagged = tagged;
                    book.lat = lat;
                    book.lon= lon;

                    db.collection("books").document(ISBN+tagged).set(book);

                    intent.putExtra("ISBN", book.isbn+tagged);
                    context.startActivity(intent);


                }
                catch(Exception e){
                    e.printStackTrace();
                    showMessageOK("Book not found");
                }


            }

        }catch(Exception e){
            showMessageOK("Book not found");
        }
    }


    @Override
    protected String doInBackground(String... strings) {
        return NetworkUtils.getBookInfo(strings[0]);
    }

    private void showMessageOK(String message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create()
                .show();
    }
}
